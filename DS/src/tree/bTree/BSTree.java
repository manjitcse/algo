package tree.bTree;

import tree.Node;

public class BSTree {
   //test
    Node root;

    public BSTree(Node root){
        this.root = root;
    }

    public void insert(int data){
        root = insertRecursively( root, data);
    }

    public Boolean search(int data, Node root){
        if(root != null)
        if( root.data == data){
            return true;
        }else{
            if(root.data>data){
                return search(data,root.left);
            }else{
                return search(data,root.right);
            }
        }
        return false;
    }

    public void remove(int data){
        //delete(data,root);
        removeRecusively(data, root);
    }

    private Node removeRecusively(int data, Node root) {
        if(root == null){
            return null;
        }
        if(root.data == data){

            if(root.left ==null && root.right ==null){
                return null;
            }
            if(root.right !=null ^ root.left !=null){
                return root.left!=null?root.left:root.right;
            }
            else{
                int min = findSmallest(root);
                root.data=min;
                root.left = removeRecusively(min,root.left);
                return root;
            }

        }else{
            if(root.data>data){
                root.left=removeRecusively(data,root.left);
            }else{
                root.right=removeRecusively(data,root.right);
            }
            return root;
        }
    }

    private void delete(int data, Node root) {

        Node parent = findkeyParent( data, root);

//        if(parent == null){
//            this.root = null;
//            return;
//        }
        Node keyNode = parent.left != null && parent.left.data==data ? parent.left:parent.right;
        //if there is no child.
        if(keyNode.left == null && keyNode.right ==null){
            if(parent.left != null && parent.left.data ==data){
                parent.left=null;
            }else{
                parent.right=null;
            }
            return;
        }
        //if there is one child
        if(keyNode.left != null ^ keyNode.right !=null){
            if(keyNode.left != null ){

                if(parent.left != null && parent.left.data ==data){
                    parent.left=keyNode.left;
                }else{
                    parent.right=keyNode.left;
                }

            }else{
                if(parent.left != null && parent.left.data ==data){
                    parent.left=keyNode.right;
                }else{
                    parent.right=keyNode.right;
                }

            }

        }

        // if there is two children
        else{
            int k = findSmallest(keyNode);
            keyNode.data = k;
            delete(k,keyNode);
        }



    }




    private int findSmallest(Node keyNode) {
        while(keyNode.left!=null){
            keyNode=keyNode.left;
        }
        return keyNode.data;
    }

    private Node findkeyParent(int data, Node child) {
        if(data== root.data){
            return null;
        }

        if((child.left !=null && child.left.data==data) || (child.right!=null && child.right.data==data)){
            return child;
        }

        if(child.left !=null && child.data>data){
            return findkeyParent(data ,child.left);
        }
        if(child.right!=null && child.data<data){
            return findkeyParent(data, child.right);
        }
        return null;


    }

    public Node insertRecursively(Node root, int data) {
        if(root==null){
            root = new Node(data);
            return root;
        }
        if(root.data > data ){
            root.left=insertRecursively(root.left,data);
            return root;
        }else{
            root.right=insertRecursively(root.right , data);
            return root;
        }
    }

    public void display(Node root){
        if(root == null){

            return;
        }
        System.out.print(root.data + " ");
        display(root.left);
        display(root.right);
    }


}
