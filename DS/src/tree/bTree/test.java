package tree.bTree;

import tree.Node;

public class test {

    public static void main(String[] args) {
        BSTree bsTree = new BSTree(new Node(20));

        bsTree.display(bsTree.root);
        bsTree.insert(30);
        System.out.println();
        bsTree.display(bsTree.root);
        bsTree.insert(10);
        bsTree.insert(11);
        bsTree.insert(9);
//        System.out.println();
//        bsTree.display(bsTree.root);
//        bsTree.insert(40);
//        bsTree.display(bsTree.root);
//        bsTree.insert(2);
//        bsTree.display(bsTree.root);
//        bsTree.insert(11);
//        System.out.println();
//        bsTree.display(bsTree.root);
//        bsTree.insert(15);
        System.out.println();
        bsTree.display(bsTree.root);
        System.out.println(bsTree.search(15,bsTree.root));

        System.out.println(bsTree.search(4,bsTree.root));

        bsTree.remove(10);
        bsTree.display(bsTree.root);


        bsTree.remove(20);
        bsTree.display(bsTree.root);


    }
}
