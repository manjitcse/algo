package tree.heap;

import java.util.Arrays;

public class HeapTree {

    int[] h = new int[10];
    int size=1;

    public void display(){
        Arrays.stream(h).filter(n->n!=0).forEach(n->System.out.print(n+" "));
        System.out.println();
    }

    public void insert(int n){
        //h[size]=n;
        int i=size;
        while(i>1 && n > h[i/2]){
            h[i]=h[i/2];
            i=i/2;

        }
        h[i]=n;
        size++;

    }

    public void remove(){
        if(size==0)
            return;

        h[1]=h[size-1];
        h[size-1]=0;
        int k=h[1];
        int i =1;
        while(i<size ){
            int ind =h[i*2]>h[(i*2)+1]?i*2:(i*2)+1;
            if(h[i]>h[ind]){
                break;
            }
            h[i]=h[ind];
            i=ind;
        }
        h[i]=k;
        size--;

    }



}
